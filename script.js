document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent (event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

// string -> string
function parseMarkDown (content) {
	// Do your work here

	// (string, string) => string
	const wrapContent = R.curry((tag, content) => `<${tag}>${content}</${tag}>`)

	// string -> string
	const warpByPTag = wrapContent('p')

	// string -> string
	const warpByHtag = n => wrapContent(`h${n}`)

	// string -> string
	const warpByBtag = wrapContent('b')

	// string -> [string]
	const splitByNewLine = R.split('\n\n')

	const splitByStar = R.split('*')

	// [string] -> string
	const joinContent = contentArray => contentArray.reduce((contentString, subContent) => contentString + subContent, '')

	// (a, a) -> boolean
	const isEqual = R.curry((a, b) => a === b)

	// (a, a) -> boolean
	const isNotEqual = R.curry((a, b) => a !== b)

	// char -> boolean
	const isEqualStar = isEqual('*')
	const isNotEqualStar = isNotEqual('*')
	const isEqualSharp = isEqual('#')
	const isNotEqualSharp = isNotEqual('#')

	// step 1: Paragragh
	// string -> string
	const paragraphToPTag = warpByPTag

	// step 2: Header
	// string -> string
	const sharpToHTag = (content) => {
		// string -> string
		const dropSharpInContent = R.compose(joinContent, R.dropWhile(isEqualSharp))
		const dropedSharpContent = dropSharpInContent(content)
		const sharpCount = content.length - dropedSharpContent.length
		return sharpCount > 0 ? warpByHtag(sharpCount)(R.trim(dropedSharpContent)) : content
	}

	// step 3: Bold text
	// string -> string
	const starToBTag = (content) => {
		const splitContent = splitByStar(content)
		const boldContent = splitContent.map((subContent, i) => {
			if (i%2 !== 0) {
				return warpByBtag(subContent)
			}
			return subContent
		})
		return joinContent(boldContent)
	}

	const contentToTag = R.compose(paragraphToPTag, sharpToHTag, starToBTag)
	
	return joinContent(splitByNewLine(content).map(contentToTag))
}